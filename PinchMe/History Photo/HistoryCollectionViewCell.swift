//
//  HistoryCollectionViewCell.swift
//  PinchMe
//
//  Created by Ulinnuha Nabilah on 01/06/20.
//  Copyright © 2020 Ulinnuha Nabilah. All rights reserved.
//

import UIKit

class HistoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var illustationImageView: UIImageView!
    
}
