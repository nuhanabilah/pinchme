//
//  IsiChatViewController.swift
//  PinchMe
//
//  Created by Kevin Heryanto on 04/06/20.
//  Copyright © 2020 Ulinnuha Nabilah. All rights reserved.
//

import UIKit

class IsiChatViewController: UIViewController {

    @IBOutlet weak var judulKonsul: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        judulKonsul.text = String(UserDefaults.standard.integer(forKey: "terima"))
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
