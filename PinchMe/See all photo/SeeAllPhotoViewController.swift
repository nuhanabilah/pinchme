//
//  SeeAllPhotoViewController.swift
//  PinchMe
//
//  Created by Ulinnuha Nabilah on 02/06/20.
//  Copyright © 2020 Ulinnuha Nabilah. All rights reserved.
//

import UIKit

class SeeAllPhotoViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    

    @IBOutlet weak var seeAllCollectionView: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    
    let brideData = UserPhotoData()
    var arrayImage: [UserStruct] = []
    
    let cellScale: CGFloat = 0.6
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        titleLabel.text = "Pipi Kanan"
        
        let screenSize = UIScreen.main.bounds.size
        let cellWidth = floor(screenSize.width * cellScale)
        let cellHeight = floor(screenSize.height * cellScale)
        let insectX = (view.bounds.width - cellWidth) / 2
        let insectY = (view.bounds.height - cellHeight) / 2
        
        
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: cellWidth, height: cellHeight)
        seeAllCollectionView.contentInset = UIEdgeInsets(top: insectY, left: insectX, bottom: insectY, right: insectX)
        layout.minimumLineSpacing = 20
        layout.minimumInteritemSpacing = 30
        layout.itemSize = CGSize(width: 280, height: 606)
        layout.sectionInset.right = 20
        layout.scrollDirection = .horizontal
        seeAllCollectionView.collectionViewLayout = layout
        
        for index in 0..<brideData.arrayOfUserPhoto.count{
            let dataObj = brideData.arrayOfUserPhoto[index]
            if dataObj.part == .rightCheek{
                arrayImage.append(dataObj)
                seeAllCollectionView.reloadData()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.hidesBackButton = true
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        arrayImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SeeAllPhotoCollectionViewCell", for: indexPath) as! SeeAllPhotoCollectionViewCell
        cell.photoImageView.image = arrayImage[indexPath.item].photo
        cell.dateTakenLabel.text = arrayImage[indexPath.item].dateTaken
        return cell
    }
    
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let layout = self.seeAllCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let cellWidthIncludingSpacing = layout.itemSize.width + layout.minimumLineSpacing
        
        var offset = targetContentOffset.pointee
        let index = (offset.x + scrollView.contentInset.left) / cellWidthIncludingSpacing
        let roundedIndex = round(index)
        
        offset = CGPoint(x: roundedIndex * cellWidthIncludingSpacing - scrollView.contentInset.left, y: scrollView.contentInset.top)
        targetContentOffset.pointee = offset
    }
    
}
