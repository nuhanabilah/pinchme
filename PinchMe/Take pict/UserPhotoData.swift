//
//  UserPhotoData.swift
//  PinchMe
//
//  Created by Ulinnuha Nabilah on 28/05/20.
//  Copyright © 2020 Ulinnuha Nabilah. All rights reserved.
//

import Foundation
import UIKit

struct UserStruct {
    var part: Part
    var photo: UIImage
    var dateTaken : String
}

enum Part: String{
    case face = "Muka"
    case forehead = "Dahi"
    case rightCheek = "Pipi Kanan"
    case leftCheek = "Pipi Kiri"
}
class UserPhotoData {
    var arrayOfUserPhoto : [UserStruct] = [UserStruct(part: .face, photo: UIImage(named: "circle_wajah")!, dateTaken: "27.05.2020"),
                                           UserStruct(part: .forehead, photo: UIImage(named: "circle_wajah")!, dateTaken: "27.05.2020"),
                                           UserStruct(part: .rightCheek, photo: UIImage(named: "pipi_kanan1")!, dateTaken: "25.02.2020"),
                                           UserStruct(part: .rightCheek, photo: UIImage(named: "pipi_kanan2")!, dateTaken: "14.04.2020"),
                                           UserStruct(part: .rightCheek, photo: UIImage(named: "pipi_kanan3")!, dateTaken: "27.05.2020"),
    ]
}

