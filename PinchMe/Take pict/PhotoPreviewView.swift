//
//  PhotoPreviewView.swift
//  PinchMe
//
//  Created by Ulinnuha Nabilah on 27/05/20.
//  Copyright © 2020 Ulinnuha Nabilah. All rights reserved.
//

import UIKit
import Photos

class PhotoPreviewView: UIView {
    
    var bridgeData = UserPhotoData()
    var arrayOfPhoto:[UserStruct] = []
    
    var statusPart: Part?
    var tempData: Data?
    
    var bridgeCustomCamera = CustomCameraViewController()
  
    let photoImageView: UIImageView = {
           let imageView = UIImageView(frame: .zero)
           imageView.contentMode = .scaleAspectFill
           imageView.clipsToBounds = true
           return imageView
       }()
       
    lazy private var cancelButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(systemName: "xmark"), for: .normal)
        button.addTarget(self, action: #selector(handleCancel), for: .touchUpInside)
        button.tintColor = .white
        return button
    }()
       
    lazy private var savePhotoButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(systemName: "square.and.arrow.down"), for: .normal)
        button.addTarget(self, action: #selector(handleSavePhoto), for: .touchUpInside)
        button.tintColor = .white
        return button
    }()
    
    lazy private var useThisPhotoButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Use photo", for: UIControl.State.init())
        button.addTarget(self, action: #selector(handleUsePhoto), for: .touchUpInside)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubviews(photoImageView, cancelButton, savePhotoButton, useThisPhotoButton)

        photoImageView.makeConstraints(top: topAnchor, left: leftAnchor, right: rightAnchor, bottom: bottomAnchor, topMargin: 0, leftMargin: 0, rightMargin: 0, bottomMargin: 0, width: 0, height: 0)
               
        cancelButton.makeConstraints(top: safeAreaLayoutGuide.topAnchor, left: nil, right: rightAnchor, bottom: nil, topMargin: 15, leftMargin: 0, rightMargin: 10, bottomMargin: 0, width: 50, height: 50)
               
        savePhotoButton.makeConstraints(top: nil, left: nil, right: cancelButton.leftAnchor, bottom: nil, topMargin: 0, leftMargin: 0, rightMargin: 5, bottomMargin: 0, width: 50, height: 50)
               savePhotoButton.centerYAnchor.constraint(equalTo: cancelButton.centerYAnchor).isActive = true
            
        useThisPhotoButton.makeConstraints(top: nil, left: nil, right: nil, bottom: photoImageView.safeAreaLayoutGuide.bottomAnchor, topMargin: 0, leftMargin: 0, rightMargin: 0, bottomMargin: 15, width: 80, height: 80)
        useThisPhotoButton.centerXAnchor.constraint(equalTo: photoImageView.centerXAnchor).isActive = true
    }
       
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
       
    @objc private func handleCancel() {
        DispatchQueue.main.async {
            self.removeFromSuperview()
        }
    }
       
    @objc private func handleSavePhoto() {
        
        guard let previewImage = self.photoImageView.image else { return }
        
        PHPhotoLibrary.requestAuthorization { (status) in
            if status == .authorized {
                do {
                    try PHPhotoLibrary.shared().performChangesAndWait {
                        PHAssetChangeRequest.creationRequestForAsset(from: previewImage)
                        print("photo has saved in library...")
                        self.handleCancel()
                    }
                } catch let error {
                    print("failed to save photo in library: ", error)
                }
            } else {
                print("Something went wrong with permission...")
            }
        }
    }
    
    @objc private func handleUsePhoto(){
        
        print("status part: \(statusPart)")
        print("image data: \(tempData)")
        
        switch statusPart {
        case .face:
            UserDefaults.standard.set(tempData, forKey: "face")
            
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd.MM.yyyy"
            let imageDate = formatter.string(from: date)
            UserDefaults.standard.set(imageDate, forKey: "imageDate")
            
            let convertImage = UIImage(data: tempData!)
            
            var imageArray: [UserStruct] = [
                UserStruct(part: .face, photo: convertImage!, dateTaken: imageDate)
            ]
            print("imageArray.count : \(imageArray.count)")

            bridgeData.arrayOfUserPhoto.append(contentsOf: imageArray)
            
            do {
                let encodeData = try NSKeyedArchiver.archivedData(withRootObject: imageArray, requiringSecureCoding: false)
                let userDefault = UserDefaults.standard
                userDefault.set(encodeData, forKey: "arrayOfFace")
            } catch let error {
                print("failed to archived data")
            }
             
            
            
//            if let imageTemp = UserDefaults.standard.array(forKey: "faceArray"){
//                imageArray = imageTemp as! [UIImage]
//                imageArray.append(convertImage!)
//            }
//            imageArray.append(tempData)
//            UserDefaults.standard.set(imageArray, forKey: "faceArray")
//
            
            //cek isi array
            for index in 0..<bridgeData.arrayOfUserPhoto.count{
                print("bridge data :")
                print(bridgeData.arrayOfUserPhoto)
            }
            
            
        case .forehead:
            UserDefaults.standard.set(tempData, forKey: "forehead")
            
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd.MM.yyyy"
            let imageDate = formatter.string(from: date)
            UserDefaults.standard.set(imageDate, forKey: "imageDate")
            
            let convertImage = UIImage(data: tempData!)
            
            var imageArray: [UserStruct] = [
                UserStruct(part: .face, photo: convertImage!, dateTaken: imageDate)
            ]
            print("imageArray.count : \(imageArray.count)")

            bridgeData.arrayOfUserPhoto.append(contentsOf: imageArray)
            
        case .leftCheek:
            UserDefaults.standard.set(tempData, forKey: "leftCheek")
            
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd.MM.yyyy"
            let imageDate = formatter.string(from: date)
            UserDefaults.standard.set(imageDate, forKey: "imageDate")
            
            let convertImage = UIImage(data: tempData!)
            
            var imageArray: [UserStruct] = [
                UserStruct(part: .face, photo: convertImage!, dateTaken: imageDate)
            ]
            print("imageArray.count : \(imageArray.count)")

            bridgeData.arrayOfUserPhoto.append(contentsOf: imageArray)
            
        case .rightCheek:
            UserDefaults.standard.set(tempData, forKey: "rightCheek")
            
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd.MM.yyyy"
            let imageDate = formatter.string(from: date)
            UserDefaults.standard.set(imageDate, forKey: "imageDate")
            
            let convertImage = UIImage(data: tempData!)
            
            var imageArray: [UserStruct] = [
                UserStruct(part: .face, photo: convertImage!, dateTaken: imageDate)
            ]
            print("imageArray.count : \(imageArray.count)")

            bridgeData.arrayOfUserPhoto.append(contentsOf: imageArray)
            
        default:
            break
        }
        
        DispatchQueue.main.async {
            self.removeFromSuperview()
        }
    }
 
}
