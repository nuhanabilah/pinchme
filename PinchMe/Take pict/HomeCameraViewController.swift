//
//  HomeCameraViewController.swift
//  PinchMe
//
//  Created by Ulinnuha Nabilah on 27/05/20.
//  Copyright © 2020 Ulinnuha Nabilah. All rights reserved.
//

import UIKit
import AVFoundation

class HomeCameraViewController: UIViewController {
    
    var brigeData = UserPhotoData()
    var statusPart: Part?
    var cekLogin: Bool = Bool()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func takeFacePhoto(_ sender: Any) {
        statusPart = .face
        
        let controller = CustomCameraViewController()
        controller.statusPart = statusPart
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func takeForeheadPhoto(_ sender: Any) {
        statusPart = .forehead
        
        let controller = CustomCameraViewController()
        controller.statusPart = statusPart
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func takeRightCheekPhoto(_ sender: Any) {
        statusPart = .rightCheek
        
        let controller = CustomCameraViewController()
        controller.statusPart = statusPart
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func takeLeftCheekPhoto(_ sender: Any) {
        statusPart = .leftCheek
        
        let controller = CustomCameraViewController()
        controller.statusPart = statusPart
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func handleSendData(_ sender: Any) {
//        if cekLogin == true {
//            var counter = UserDefaults.standard.integer(forKey: "counter") ?? 0
//            UserDefaults.standard.set( counter + 1 , forKey: "counter")
//            print(UserDefaults.standard.integer(forKey: "counter"))
//        }else {
        alertDataDiri(title: "Data Diri", message: "Untuk memudahkan konsultasi, silakan mengisi data diri kamu terlebih dahulu.")
//        }
        
        
        
        
        
        
        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "IdentifyViewController") as! IdentifyViewController
//
//        vc.stepOneCheck = true
//        vc.stepTwoCheck = true
//
//        self.navigationController!.pushViewController(vc, animated: true)
    }
    
    func alertDataDiri(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(ACTION) in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "halamanLogin") as! halamanLogin
            self.navigationController?.pushViewController(vc, animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
}
