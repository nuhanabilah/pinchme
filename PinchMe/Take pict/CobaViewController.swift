//
//  CobaViewController.swift
//  PinchMe
//
//  Created by Ulinnuha Nabilah on 28/05/20.
//  Copyright © 2020 Ulinnuha Nabilah. All rights reserved.
//

import UIKit

class CobaViewController: UIViewController {
    
    @IBOutlet weak var faceImageView: UIImageView!
    @IBOutlet weak var foreheadImageView: UIImageView!
    @IBOutlet weak var imageDateLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        imageDateLabel.text = UserDefaults.standard.object(forKey: "imageDate") as! String
        
        let imageData = UserDefaults.standard.object(forKey: "face")
        var image = UIImage(data: imageData as! Data)
        faceImageView.image = image
            
        let imageData2 = UserDefaults.standard.object(forKey: "forehead")
        print(imageData2)
        var image2 = UIImage(data: imageData2 as! Data)
        foreheadImageView.image = image2
          
    }
}
