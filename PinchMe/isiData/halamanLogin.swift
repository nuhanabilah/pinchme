//
//  halamanLogin.swift
//  PinchMe
//
//  Created by Kevin Heryanto on 27/05/20.
//  Copyright © 2020 Ulinnuha Nabilah. All rights reserved.
//

import UIKit

class halamanLogin: UIViewController {

    @IBOutlet weak var inputNama: UITextField!
    @IBOutlet weak var inputTelepon: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        print(UserDefaults.standard.string(forKey: "namaUser"))
        print(UserDefaults.standard.string(forKey: "nomorUser"))
        
    }
    
    
    @IBAction func Masuk(_ sender: UIButton) {
        if (inputNama.text == UserDefaults.standard.string(forKey: "namaUser") && inputTelepon.text == UserDefaults.standard.string(forKey: "nomorUser")) {
            performSegue(withIdentifier: "masukSini", sender: nil)
            print("halo")
        }else {
            displayAlertMessage(userMessage: "Data yang dimasukkan salah")
            return;
        }
    }
    
    func displayAlertMessage(userMessage: String) {
        
        var Alertnya = UIAlertController(title:"Alert", message: userMessage, preferredStyle: UIAlertController.Style.alert);
        
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)
        
        Alertnya.addAction(okAction)
        
        self.present(Alertnya, animated:  true, completion: nil)
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
