//
//  userProfile.swift
//  PinchMe
//
//  Created by Kevin Heryanto on 01/06/20.
//  Copyright © 2020 Ulinnuha Nabilah. All rights reserved.
//

import UIKit

class userProfile: UIViewController {

    
    
    @IBOutlet weak var namaUser: UILabel!
    @IBOutlet weak var nomorUser: UILabel!
    @IBOutlet weak var emailUser: UILabel!
    @IBOutlet weak var lahirUser: UILabel!
    @IBOutlet weak var genreUser: UILabel!
    
    
    
    
    
    let nama = UserDefaults.standard.string(forKey: "namaUser")
    let nomor = UserDefaults.standard.string(forKey: "nomorUser")
    let email = UserDefaults.standard.string(forKey: "emailUser")
    let lahir = UserDefaults.standard.string(forKey: "TanggalLahir")
    let genre = UserDefaults.standard.string(forKey: "kelaminUser")
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        namaUser.text = nama
               nomorUser.text = nomor
               emailUser.text = email
               lahirUser.text = lahir
               genreUser.text = genre
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
