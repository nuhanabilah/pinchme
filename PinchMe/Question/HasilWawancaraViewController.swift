//
//  HasilWawancaraViewController.swift
//  PinchMe
//
//  Created by Faisal on 28/05/20.
//  Copyright © 2020 Ulinnuha Nabilah. All rights reserved.
//

import UIKit

class HasilWawancaraViewController: UIViewController {

    @IBOutlet weak var jawaban1: UILabel!
    @IBOutlet weak var jawaban2: UILabel!
    @IBOutlet weak var jawaban3: UILabel!
    @IBOutlet weak var jawaban4: UILabel!
    @IBOutlet weak var jawaban5: UILabel!
    @IBOutlet weak var jawaban6: UILabel!
    @IBOutlet weak var jawaban7: UILabel!
    var Question1 = ""
    var Question2 = ""
    var Question3 = ""
    var Question4 = ""
    var Question5 = ""
    var Question6 = ""
    var Question7 = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        jawaban1.text = Question1
        jawaban2.text = Question2
        jawaban3.text = Question3
        jawaban4.text = Question4
        jawaban5.text = Question5
        jawaban6.text = Question6
        jawaban7.text = Question7

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
