//
//  Question6ViewController.swift
//  PinchMe
//
//  Created by Faisal on 25/05/20.
//  Copyright © 2020 Ulinnuha Nabilah. All rights reserved.
//

import UIKit

class Question6ViewController: UIViewController {

    @IBOutlet weak var LabelQuestion6: UITextField!
    var Question1 = ""
    var Question2 = ""
    var Question3 = ""
    var Question4 = ""
    var Question5 = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        print(Question1)
        print(Question2)
        print(Question3)
        print(Question4)
        print(Question5)

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    

    @IBAction func unwindToScreenBefore (_ unwindSegue: UIStoryboardSegue) {

    }
    @IBAction func nextButton(_ sender: UIButton) {
        performSegue(withIdentifier: "toQuestion7", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is Question7ViewController{
            if let passData = segue.destination as? Question7ViewController{
                passData.Question1 = Question1
                passData.Question2 = Question2
                passData.Question3 = Question3
                passData.Question4 = Question4
                passData.Question5 = Question5
                passData.Question6 = LabelQuestion6.text!
            }
        }
//        if segue.destination is Question5ViewController{
//                              let trans = CATransition()
//                           trans.type = CATransitionType.push
//                           trans.subtype = CATransitionSubtype.fromLeft
//                           trans.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//                           trans.duration = 0.3
//                              self.navigationController?.view.layer.add(trans, forKey: nil)
//                          }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
