//
//  Question2ViewController.swift
//  PinchMe
//
//  Created by Faisal on 25/05/20.
//  Copyright © 2020 Ulinnuha Nabilah. All rights reserved.
//

import UIKit

class Question2ViewController: UIViewController {

    @IBOutlet weak var LabelQuestion2: UITextField!
    var Question1 = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        print(Question1)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction func nextButton(_ sender: UIButton) {
        performSegue(withIdentifier: "toQuestion3", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is Question3ViewController{
            if let passData = segue.destination as? Question3ViewController{
                passData.Question1 = Question1
                passData.Question2 = LabelQuestion2.text!
            }
        }
//        if segue.destination is Question1ViewController{
//                       let trans = CATransition()
//                    trans.type = CATransitionType.push
//                    trans.subtype = CATransitionSubtype.fromLeft
//                    trans.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//                    trans.duration = 0.3
//                       self.navigationController?.view.layer.add(trans, forKey: nil)
//                   }
    }
    
    @IBAction func unwindToScreenBefore (_ unwindSegue: UIStoryboardSegue) {

    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//           if let _ = segue.destination as? Question1ViewController{
//               let trans = CATransition()
//            trans.type = CATransitionType.moveIn
//            trans.subtype = CATransitionSubtype.fromLeft
//            trans.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//            trans.duration = 0
//               self.navigationController?.view.layer.add(trans, forKey: nil)
//           }
//       }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
